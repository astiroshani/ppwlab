from django.shortcuts import render

# Create your views here.
response = {}

def index(request):
    return render(request,'index_lab1.html',response)

def home(request):
    return render(request,'index.html',response)