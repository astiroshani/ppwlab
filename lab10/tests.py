from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from django.test import LiveServerTestCase
from django.db import IntegrityError
from .views import subscribe, checkEmail
from .models import Subscriber
from .forms import FormSubscriber
import unittest

# Create your tests here.
class Lab10_Test(TestCase):
    def test_lab10_url_is_exist(self):
        response = Client().get('/lab10/')
        self.assertEqual(response.status_code, 200)

    def test_lab10_using_subscribe_template(self):
        response = Client().get('/lab10/')
        self.assertTemplateUsed(response, 'subscribe.html')

    def test_lab10_using_subscribe_func(self):
        found = resolve('/lab10/')
        self.assertEqual(found.func, subscribe)

    def test_lab10_using_checkEmail_func(self):
        found = resolve('/lab10/check-email/')
        self.assertEqual(found.func, checkEmail)

    def test_model_can_create_new_subscriber(self):
        new_subscriber = Subscriber.objects.create(name="me", email="itsme@yahoo.com", password="hello1234")
        counting_all_subscriber = Subscriber.objects.all().count()
        self.assertEqual(counting_all_subscriber, 1)

    def test_FormSubscriber_valid(self):
        form = FormSubscriber(data={'name': "me", 'email': "user@yahoo.com" , 'password': "user1234"})
        self.assertTrue(form.is_valid())
    
    def test_max_length_name(self):
        name = Subscriber.objects.create(name="paling banyak 30 karakter ya")
        self.assertLessEqual(len(str(name)), 30)
    
    def test_unique_email(self):
        Subscriber.objects.create(email="asti@email.com")
        with self.assertRaises(IntegrityError):
            Subscriber.objects.create(email="asti@email.com")
        
    def test_check_email_view_get_return_200(self):
        email = "asti@gmail.com"
        Client().post('/lab10/check-email/', {'email': email})
        response = Client().post('/lab10/', {'email': 'asti@gmail.com'})
        self.assertEqual(response.status_code, 200)

    def test_check_email_already_exist_view_get_return_200(self):
        Subscriber.objects.create(name="me", email="yooo@gmail.com", password="abcde")
        response = Client().post('/lab10/check-email/', {
            "email": "yooo@gmail.com"
        })
        self.assertEqual(response.json()['is_email'], True)

    def test_subscribe_should_return_status_subscribe_true(self):
        response = Client().post('/lab10/', {
            "name": "capekbeneran",
            "email": "asti@gmail.com",
            "password":  "password",
        })
        self.assertEqual(response.json()['status_subscribe'], True)

    def test_subscribe_should_return_status_subscribe_false(self):
        name, email, password = "asti", "asti@gmail.com", "password"
        Subscriber.objects.create(name=name, email=email, password=password)
        response = Client().post('/lab10/', {
            "name": name,
            "email": email,
            "password":  password,
        })
        self.assertEqual(response.json()['status_subscribe'], False)

