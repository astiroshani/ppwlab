from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from django.test import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from .views import ubahtema
import unittest
import time

# Create your tests here.
class Lab8_Test(TestCase):
    def test_lab_8_url_is_exist(self):
        response = Client().get('/lab8/')
        self.assertEqual(response.status_code, 200)

    def test_lab8_using_ubahtema_template(self):
        response = Client().get('/lab8/')
        self.assertTemplateUsed(response, 'ubahtema.html')

    def test_lab8_using_ubahtema_func(self):
        found= resolve('/lab8/')
        self.assertEqual(found.func, ubahtema)
    
    def test_lab8_aboutme_header(self):
        request = HttpRequest()
        response = ubahtema(request)
        html_response = response.content.decode('utf8')
        self.assertIn('<strong>About Me</strong>', html_response)

    def test_lab8_aboutme_description(self):
        request = HttpRequest()
        response = ubahtema(request)
        html_response = response.content.decode('utf8')
        self.assertIn('The owner of this website is just your friendly computer science student. She said Hi!', html_response)


class Lab8FunctionalTest(LiveServerTestCase):
    def setUp(self):
        op = Options()
        op.add_argument('--dns-prefetch-disable')
        op.add_argument('--no-sandbox')
        op.add_argument('--headless')
        op.add_argument('disable-gpu')
        self.browser = webdriver.Chrome(executable_path='./chromedriver', chrome_options=op) 
        super(Lab8FunctionalTest, self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(Lab8FunctionalTest, self).tearDown()

    def test_layout_page_title(self):
        browser = self.browser
        browser.get('http://ppw-c-roshani.herokuapp.com/lab8/')
        self.assertIn('hello', self.browser.title)
        time.sleep(2)

    def test_layout_header_with_css_property(self):
        browser = self.browser
        browser.get('http://ppw-c-roshani.herokuapp.com/lab8/')
        content = browser.find_element_by_tag_name('p').value_of_css_property('color')
        self.assertIn('rgba(0, 0, 0, 1)', content)
        time.sleep(2)