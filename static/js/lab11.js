$(document).ready(function() {
	checkLike();
	$("#myInput").on("keyup", function(e) {
		q = e.currentTarget.value.toLowerCase()
		console.log(q)

		$.ajax({
			url: "json/?q=" + q,
			datatype: 'json',
			success: function(data){
				// console.log(data);
				$('tbody').html('')
				var result ='<tr>';
				for(var i = 0; i < data.items.length; i++) {
					result += "<th scope='row' class='align-middle text-center'>" + (i+1) + "</th>" +
					"<td><img class='img-fluid' style='width:22vh' src='" + data.items[i].volumeInfo.imageLinks.smallThumbnail + "'></img>" + "</td>" +
					"<td class='align-middle'>" + data.items[i].volumeInfo.title + "</td>" +
					"<td class='align-middle'>" + data.items[i].volumeInfo.authors + "</td>" + 
					"<td class='align-middle'>" + data.items[i].volumeInfo.publisher + "</td>" + 
					"<td class='align-middle'>" + data.items[i].volumeInfo.publishedDate +"</td>" + 
					"<td class='align-middle' style='text-align:center'>" + "<img id='" + data.items[i].id + "' onclick='favorite(this.id)' width='28px' src='https://image.flaticon.com/icons/svg/149/149222.svg'>" + "</td></tr>";
				}
				$('tbody').append(result);
				checkLike();
			},
			error: function(error){
				$('#alert').html('')
				var result = "Server error :(";
				$('#alert').append(result);
			}
		})
	});
});

var counter = 0;
function checkLike(){
	$.ajax({
		type: 'GET',
		url: '/lab11/book-list/get-like/',
		dataType: 'json',
		success: function(data) {

			for (var i=1; i <= data.message.length; i++) {
				console.log(data.message[i-1]);
				var id = data.message[i-1];
				var td = document.getElementById(id);
				if (td!=null) {
					td.className = 'clicked';
					td.src = 'https://image.flaticon.com/icons/svg/291/291205.svg';
				}  
				$('#counter').html(data.message.length);	  
			}
			$('tbody').html(print);
		}
	});  
};
  
function favorite(id) {
	var csrftoken = $("[name=csrfmiddlewaretoken]").val();
	var ini = document.getElementById(id);
	var yellowstar = 'https://image.flaticon.com/icons/svg/291/291205.svg';
	var blankstar = 'https://image.flaticon.com/icons/svg/149/149222.svg';
	if (ini.className=='checked') {
		$.ajax({
			url: "/lab11/book-list/unlike/",
			type: "POST",
			headers: {
				"X-CSRFToken": csrftoken,
			},	
			data: {
			id: id,
			},
			success: function(result) {
				counter=result.message;
				ini.className='';
				ini.src=blankstar;
				$('#counter').html(counter);
			},
			error : function (errmsg){
				alert("Something is wrong");
			}
		});
	} else {
		$.ajax({
			url: "/lab11/book-list/like/",
			type: "POST",
			headers: {
				"X-CSRFToken": csrftoken,
			},	
			data: {
				id: id,
			},
			success: function(result) {
				console.log(ini);
				counter=result.message;
				ini.className='checked';
				ini.src=yellowstar;
				$('#counter').html(counter);
			   
		   },
		   error : function (errmsg){
			   alert("Something is wrong");
		   }
		});
	}
}

// var counter = 0;
// function favorite(clicked_id){
// 	var btn = document.getElementById(clicked_id);
// 	if(btn.classList.contains("checked")){
// 		btn.classList.remove("checked");
// 		document.getElementById(clicked_id).src = 'https://image.flaticon.com/icons/svg/149/149222.svg';
// 		counter--;
// 		document.getElementById("counter").innerHTML = counter;
// 	}
// 	else{
// 		btn.classList.add('checked');
// 		document.getElementById(clicked_id).src = 'https://image.flaticon.com/icons/svg/291/291205.svg';
// 		counter++;
// 		document.getElementById("counter").innerHTML = counter;
// 	}
// }